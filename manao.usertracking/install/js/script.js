BX.ready(function () {

    BX.userTracking = {
        pathToAjax: null,
        tasksList: new BX.PopupWindow(
            "user_tasks_list",
            null,
            {
                closeIcon: {right: "10px", top: "10px"},
                titleBar: {
                    content: BX.create("span", {
                        html: '<b>Трекинг пользователей</b>',
                        'props': {'className': 'access-title-bar'}
                    })
                },
                zIndex: 0,
                offsetLeft: 0,
                offsetTop: 0,
                closeByEsc: true,
                autoHide: true,
                overlay: {backgroundColor: 'black', opacity: '80' },
                draggable: {restrict: false},
                buttons: [
                    new BX.PopupWindowButton({
                        text: "Обновить",
                        className: "popup-window-button-accept",
                        events: {
                            click: function () {
                                BX.ajax.get('/bitrix/components/manao/usertracking/ajax.php?ajax=Y', function(data) {
                                    BX.userTracking.tasksList.setContent(data);
                                });
                                //BX.ajax.insertToNode('/bitrix/components/manao/usertracking/ajax.php?ajax=Y', BX('user_tasks_popup'));
                            }
                        }
                    })
                ]
            })
    }
    $('#showUsersTrackingPopup').click(function () {
        BX.ajax.get('/bitrix/components/manao/usertracking/ajax.php?ajax=Y', function(data) {
            BX.userTracking.tasksList.setContent(data);
            BX.userTracking.tasksList.show();
        });
       // BX.ajax.insertToNode('/bitrix/components/manao/usertracking/ajax.php?ajax=Y', BX('user_tasks_popup'));
        //BX.userTracking.tasksList.show();
    });
});