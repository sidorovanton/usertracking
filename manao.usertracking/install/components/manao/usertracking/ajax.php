<?
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php');
global $APPLICATION;

CBitrixComponent::includeComponentClass('manao:usertracking');

$component = new CBitrixComponent();
$component->InitComponent('manao:usertracking', '');
$APPLICATION->IncludeComponent('manao:usertracking', '', array(), $component);