<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Web\Json;

Loc::loadMessages(__FILE__);

class UsersTrackingComponent extends \CBitrixComponent
{
    public function onPrepareComponentParams($arParams){
        if(!is_array($arParams['EXCLUDE_DEPARTAMENTS']))
            $arParams['EXCLUDE_DEPARTAMENTS'] = array();

        return $arParams;
    }

    protected function prepareData()
    {

        return true;
    }

    public function executeComponent()
    {
        global $APPLICATION, $DB;

        if (!CModule::IncludeModule('tasks'))
            return false;

        $this->getStructureDepartaments();
        $arUsers = array();

        $rsUser = CUser::GetList(
            $sort = 'ID',
            $order = 'ASC',
            array(
                'ACTIVE' => 'Y',
                '!UF_DEPARTMENT' => array_merge(array(false), $this->arParams['EXCLUDE_DEPARTAMENTS'])
            )
        );

        while ($arUser = $rsUser->Fetch()) {
            $arUsers[$arUser['ID']] = array(
                'IS_TRACKED' => 0,
                'INFO' => $arUser
            );
        }

        $res = Bitrix\Tasks\Internals\Task\TimerTable::getList([
           'select' => [
              'TASK_ID','USER_ID', 'TIMER_STARTED_AT', 'TIMER_ACCUMULATOR'
           ],
           'filter' => [
              '!TIMER_STARTED_AT' => 0 ,
           ]
        ]);

        $arTasksId = array();
        while ($row = $res->Fetch()) {
            $arTasksId[] = $row['TASK_ID'];
            $arUsers[$row['USER_ID']]['TASKS'][$row['TASK_ID']] = time() - $row['TIMER_STARTED_AT'];
            $arUsers[$row['USER_ID']]['IS_TRACKED'] = $row['TASK_ID'];
        }

        $arFilter = array(
            ">=CREATED_DATE" => date('d.m.Y') . ' 00:00:00',
            "<=CREATED_DATE" => date('d.m.Y') . ' 23:59:59',
            'USER_ID' => array_keys($arUsers)
        );

        $res = CTaskElapsedTime::GetList(
            Array(),
            $arFilter
        );

        while ($arElapsed = $res->Fetch()) {
            $arTasksId[] = $arElapsed['TASK_ID'];
            $arUsers[$arElapsed['USER_ID']]['TASKS'][$arElapsed['TASK_ID']] += $arElapsed['SECONDS'];
        }

        foreach($arUsers as $id => $arUser){
            $iUserTiming = array_sum($arUser['TASKS']);
            $arUsers[$id]['TIME'] = $iUserTiming;
            $arUsers[$id]['TIME_PRINT'] = sprintf(
                '%02d'.GetMessage('MANAO_UT_COMPONENT_HOUR').'. %02d'.GetMessage('MANAO_UT_COMPONENT_MINUTES').'. %02d'.GetMessage('MANAO_UT_COMPONENT_SECONDS').'.',
                floor($iUserTiming / 3600),    // hours
                floor($iUserTiming / 60) % 60,    // minutes
                floor($iUserTiming) % 60    // minutes
            );
        }

        $this->arResult['USERS'] = $arUsers;

        $this->arResult['TASKS'] = array();
        if (count($arTasksId) > 0) {
            $rsTasks = CTasks::GetList(array(), array('ID' => $arTasksId));
            while ($task = $rsTasks->Fetch()) {
                $this->arResult['TASKS'][$task['ID']] = $task;
            }
        }

        $isAjaxRequest = $this->request["ajax"] == "Y";

        if ($isAjaxRequest)
            $APPLICATION->RestartBuffer();

        // if (!$isAjaxRequest) {
        //     CJSCore::RegisterExt('user_tracking', array(
        //         'js' => '/bitrix/js/manao.usertracking/script.js',
        //         'css' => '/bitrix/components/manao/usertracking/templates/.default/style.css',
        //         'rel' => array('popup', 'jquery', 'ajax')
        //     ));
        //     CJSCore::Init(array('user_tracking'));
        // }

        $this->includeComponentTemplate();

        if ($isAjaxRequest) {
            $APPLICATION->FinalActions();
            die();
        }

    }

    protected function getStructureDepartaments(){
        $arStructure = CIntranetUtils::GetStructure();
        foreach($arStructure['DATA'] as &$arDepartament){
            $arDepartament['SHOW'] = in_array($arDepartament['ID'], $this->arParams['EXCLUDE_DEPARTAMENTS']) ? 0 : 1;
        }

        $this->arResult['STRUCTURE'] = $arStructure['DATA'];
    }
}
