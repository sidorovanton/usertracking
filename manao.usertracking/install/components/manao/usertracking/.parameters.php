<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
use Bitrix\Main\Loader;

if (!Loader::includeModule('intranet'))
	return;

$arStructure = CIntranetUtils::GetStructure();
foreach($arStructure['DATA'] as $departament){
	$arSettingStructure[$departament['ID']] = str_repeat('.', $departament['DEPTH_LEVEL']).$departament['NAME'];
}

$arComponentParameters = array(
	"PARAMETERS" => array(
		"EXCLUDE_DEPARTAMENTS" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("STRUCTURE_DEPARTAMENT"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"DEFAULT" => "-",
			"VALUES" => $arSettingStructure,
		)
	)
);
