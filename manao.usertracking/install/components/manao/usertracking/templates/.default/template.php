<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="modul-elapsed-time">
    <div class="modul-elapsed-time-head">
        <div class="modul-elapsed-time-name"><?=GetMessage('TITLE_EMPLOYER');?></div>
        <div class="modul-elapsed-time-time"><?=GetMessage('TITLE_TIME');?></div>
        <div class="modul-elapsed-time-task"><?=GetMessage('TITLE_TASK');?></div>
    </div>
    <? foreach ($arResult['STRUCTURE'] as $idStructure => $arDepartament): ?>
        <?
        if(!$arDepartament['SHOW'])
            continue;
        ?>

        <b><?= $arDepartament['NAME']; ?></b>
        <?
        foreach ($arDepartament['EMPLOYEES'] as $arUserId):
            $arUser = $arResult['USERS'][$arUserId];
            $bIsTracked = $arUser['IS_TRACKED'] > 0;
            ?>
            <div class="user <?= $bIsTracked ? 'active' : ''; ?>">
                <div class="modul-elapsed-time-name">
                    <i></i>
                    <?= $arUser['INFO']['LAST_NAME'] ?> <?= $arUser['INFO']['NAME']; ?>
                </div>

                <div class="modul-elapsed-time-time">
                    <?=$arUser['TIME_PRINT']?>
                </div>
                <div class="modul-elapsed-time-task">
                    <?if($bIsTracked):?>
                    <?$arTask = $arResult["TASKS"][$arUser['IS_TRACKED']];?>
                    <a href="/company/personal/user/<?= $arUser['ID'] ?>/tasks/task/show/<?= $arTask['ID'] ?>/"
                       target="_blank"><?= $arTask['TITLE'] ?>
                    </a>
                    <? unset($arTask);?>
                    <?endif;?>
                </div>
            </div>
        <? endforeach; ?>
    <? endforeach; ?>
</div>
