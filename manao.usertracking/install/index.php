<?php
IncludeModuleLangFile(__FILE__);

Class manao_usertracking extends CModule
{
    var $MODULE_ID = "manao.usertracking";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function manao_usertracking()
    {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = GetMessage('MANAO_UT_NAME');
        $this->PARTNER_NAME = "Manao";
        $this->PARTNER_URI = "http://manao.by";
        $this->MODULE_DESCRIPTION = GetMessage("MANAO_UT_DESCRIPTION");
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        RegisterModule("manao.usertracking");
        $this->InstallFiles();
        $APPLICATION->IncludeAdminFile(GetMessage("MANAO_UT_MODULE_INSTALLING"), $DOCUMENT_ROOT . "/bitrix/modules/manao.usertracking/install/step.php");
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/manao.usertracking/install/components",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/manao.usertracking/install/js",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/manao.usertracking", true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/components/manao");
        DeleteDirFilesEx("/bitrix/js/manao.usertracking");
        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        UnRegisterModule("manao.usertracking");
        $APPLICATION->IncludeAdminFile(GetMessage("MANAO_UT_MODULE_UNINSTALLING"), $DOCUMENT_ROOT . "/bitrix/modules/manao.usertracking/install/unstep.php");
    }
}

?>
