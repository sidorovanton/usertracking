<?php

CJSCore::RegisterExt('user_tracking', array(
    'js' => '/bitrix/js/manao.usertracking/script.js',
    'css' => '/bitrix/components/manao/usertracking/templates/.default/style.css',
    'rel' => array('popup', 'jquery', 'ajax')
));

